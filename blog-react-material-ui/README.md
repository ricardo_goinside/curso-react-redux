# Integração do Material UI com ReactJS

https://blog.rocketseat.com.br/react-material-ui/

## Instalação

Utilize os comandos:

<code>cd blog-react-material-ui<BR>
npm install<BR>
npm start</code>

Acesse a página no link: http://localhost:3000

## Curso React-Redux

### Requisitos

Baixar Node.js: <url> https://nodejs.org/pt-br/download/ </url>

### Instalar e Execultar Projeto

Acessar diretório fundamentos-react

Instalando as dependências <code> npx install </code>

Execultar App <code> npm start </code>

### Outros Comando

Criar novo App React: <code> npx create-react-app nome-pasta </code>
 
Criar arquivo package.json <code> npm init </code> 

Instalação React  <code> npm install react --save </code> 
 
Instalação React-Dom  <code> npm install react-dom --save </code> 